const { chromium } = require('playwright');

const expect = require('expect');
//import "expect-playwright"

// const express = require("express");

let browser, context, page;
beforeAll(async () => {
  browser = await chromium.launch({
    headless: true
  });
  context = await browser.newContext();
});

afterAll(async () => {
  await context.close();
  await browser.close();
});

beforeEach(async () => {
  page = await context.newPage();
});

afterEach(async () => {
  await page.close();
});

test('test login page 1', async () => {

  // Go to https://www.tmbbank.com/home
  // await page.goto('https://www.tmbbank.com/home');
 
  await page.goto('https://www.tmbdirect.com/tmb/kdw1.30.1');
  await page.waitForSelector('#frmIBPreLogin_btnLogIn');
  const text = await page.$eval('#frmIBPreLogin_btnLogIn', e => e.value);
  //expect(text).toBe('เข้าสู่ระบบ');
  
  await expect(text).toBe("Log In");

});

test('test login page 2', async () => {


  await page.goto('https://www.tmbdirect.com/tmb/kdw1.30.1');
  await page.waitForSelector('#frmIBPreLogin_btnLogIn');
  const text = await page.$eval('#frmIBPreLogin_btnLogIn', e => e.value);
  //expect(text).toBe('เข้าสู่ระบบ');
  
  await expect(text).toBe("เข้าสู่ระบบ");

});
  
